<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>

</head>
<body>
<table style="width:100%">
  <tr>
    <th>Food id</th>
    <th>Food name</th>
    <th>Food price</th>
    <th>Food amount</th>
  </tr>
  <?php 
   foreach($menu->result_array() as $row){
  ?>
  <tr>
    <td><?= $row['fid']?></td>
    <td><?= $row['fname']?></td>
    <td><?= $row['fprice']?></td>
    <td><?= $row['famount']?></td>
  </tr>
<?php
}
?>
</table>
</body>
</html>