<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function showFood()
	{
		//httpp//localhost/foodpanda/index.php/Welcome/showFood
		$this->load->database();
		$sql = "SELECT * FROM food";
		$query = $this->db->query($sql)->result();
		print_r($query);
	}

	public function showFood2()
	{
		$this->load->model('food_model','fm');
		$result['menu']=$this->fm->getFood();
		$this->load->view('food_list',$result);
		//print_r($result);
	}
}
